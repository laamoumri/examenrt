package org.example.examenrt.dao;

public record DataInput(
        int BusinessEntityID,
        int NationalIDNumber,
        String LoginID) {
}
