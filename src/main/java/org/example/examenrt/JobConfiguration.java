package org.example.examenrt;

import org.example.examenrt.dao.DataInput;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jdbc.support.JdbcTransactionManager;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.retry.support.RetryTemplateBuilder;

import javax.sql.DataSource;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Configuration
public class JobConfiguration {

    @Bean
    public Job job(JobRepository jobRepository, Step step1, Step step2, Step step3) {
        return new JobBuilder("inputData", jobRepository)
                .start(step1)
                .next(step2)
                .next(step3)
                .build();
    }


    @Bean
    public Step step1(JobRepository jobRepository, JdbcTransactionManager transactionManager) {
        return new StepBuilder("copy", jobRepository)
                .tasklet(preparingStaging(), transactionManager)
                .build();
    }

    @Bean
    @StepScope
    public Tasklet preparingStaging() {
        return new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                JobParameters jobParameters = contribution.getStepExecution().getJobParameters();
                String inputFile = "src/main/resources/data.csv";
                Path source = Paths.get(inputFile);
                Path target = Paths.get("staging", source.toFile().getName());
                Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
                return RepeatStatus.FINISHED;
            }
        };
    }

    @Bean
    public FlatFileItemReader<DataInput> inputDataFileItemReader() {
        return new FlatFileItemReaderBuilder<DataInput>()
                .name("inputDataFileItemReader")
                .resource(new FileSystemResource("staging/data.csv"))
                .delimited()
                .names()
                .targetType(DataInput.class)
                .build();
    }

    @Bean
    public JdbcBatchItemWriter<DataInput> inputDataTableWriter(DataSource dataSource) {
        String sql = "insert into INPUT_DATA(:BusinessEntityID, :NationalIDNumber, :LoginID)";
        return new JdbcBatchItemWriterBuilder<DataInput>()
                .dataSource(dataSource)
                .sql(sql)
                .beanMapped()
                .build();
    }

    @Bean
    public Step step2(JobRepository jobRepository, JdbcTransactionManager transactionManager,
                      ItemReader<DataInput> dataInputItemReader, ItemWriter<DataInput> dataInputItemWriter,
                      RetryTemplate retryTemplate) {
        double chunk = Math.ceil(1000 * 0.05);

        return new StepBuilder("segmentation", jobRepository)
                .<DataInput, DataInput>chunk((int) chunk, transactionManager)
                .reader(dataInputItemReader)
                .writer(dataInputItemWriter)
                .faultTolerant()
                .retry(Exception.class)
                .skipLimit(7)
                .build();
    }


    /*private Tasklet segmentationTasklet() {
        return new Tasklet() {
            @Override
            public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {


                return RepeatStatus.FINISHED;
            }
        };
    }*/

    @Bean
    public RetryTemplate retryTemplate() {
        return new RetryTemplateBuilder()
                .maxAttempts(3)
                .build();
    }



}
